package com.sil.BFVSensing.FakeData;

public class FakeData {

    private int imageID;
    private String bloodflow;
    private String minbloodflow;

    public FakeData(int imageID, String bloodflow, String minbloodflow) {
        this.imageID = imageID;
        this.bloodflow = bloodflow;
        this.minbloodflow = minbloodflow;
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public String getBloodflow() {
        return bloodflow;
    }

    public void setBloodflow(String bloodflow) {
        this.bloodflow = bloodflow;
    }

    public String getMinbloodflow() {
        return minbloodflow;
    }

    public void setMinbloodflow(String minbloodflow) {
        this.minbloodflow = minbloodflow;
    }
}
