package com.sil.BFVSensing.FakeData;

import com.sil.BFVSensing.R;

import java.util.ArrayList;
import java.util.List;

public class FakeDataList {

    public List<FakeData> fakedatamonth(){
        List<FakeData> datamonth = new ArrayList<>();
        datamonth.add(new FakeData(R.mipmap.diagram_month_2018_11, "743", "703"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2018_12, "755", "704"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2019_1, "750", "703"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2019_2, "730", "652"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2019_3, "704", "653"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2019_4, "695", "652"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2019_5, "700", "652"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2019_6, "696", "653"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2019_7, "689", "637"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2019_8, "676", "631"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2019_9, "642", "584"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2019_10, "636", "582"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2019_11, "620", "572"));
        datamonth.add(new FakeData(R.mipmap.diagram_month_2019_12, "618", "582"));
        return datamonth ;
    }

    public List<FakeData> fakedatayear(){
        List<FakeData> datayear = new ArrayList<>();
        datayear.add(new FakeData(R.mipmap.diagram_year_2018,"752","737"));
        datayear.add(new FakeData(R.mipmap.diagram_year_2019,"682","604"));
        return datayear ;
    }
}
