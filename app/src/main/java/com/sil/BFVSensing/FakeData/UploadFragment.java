package com.sil.BFVSensing.FakeData;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.sil.BFVSensing.R;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class UploadFragment extends Fragment {
    private View view;
    private TextView mMonth, mYear, mAverageBloodFlow, mMinBloodFlow, mBloodFlowValue1, mBloodFlowValue2;
    private ImageView mImageViewMonth, mImageViewYear, mFakeImage;
    private Button btnChangeMonth, btnChangeYear;
    int pickerSelectNumber;


    public UploadFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_upload, container, false);
        mMonth = (TextView) view.findViewById(R.id.tv_month);
        mYear = (TextView) view.findViewById(R.id.tv_year);
        mImageViewMonth = (ImageView) view.findViewById(R.id.iv_underline_month);
        mImageViewYear = (ImageView) view.findViewById(R.id.iv_underline_year);
        mFakeImage = (ImageView) view.findViewById(R.id.iv_fakeData);
        btnChangeMonth = (Button) view.findViewById(R.id.btn_chooseMonth);
        btnChangeYear = (Button) view.findViewById(R.id.btn_chooseYear);
        mAverageBloodFlow = (TextView) view.findViewById(R.id.tv_averageBloodFlow);
        mMinBloodFlow = (TextView) view.findViewById(R.id.tv_minBloodFlow);
        mBloodFlowValue1 = (TextView) view.findViewById(R.id.tv_bloodflowvalue1);
        mBloodFlowValue2 = (TextView) view.findViewById(R.id.tv_bloodflowvalue2);

        mFakeImage.setImageResource(R.mipmap.diagram_month_2018_11);
        btnChangeMonth.setText(getResources().getStringArray(R.array.array_month)[0]);
        mBloodFlowValue1.setText("743");
        mBloodFlowValue2.setText("703");

        mMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mYear.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTransparentBlack));
                mImageViewYear.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorTransparentBlack));
                mMonth.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                mImageViewMonth.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                btnChangeMonth.setText(getResources().getStringArray(R.array.array_month)[0]);
                btnChangeMonth.setVisibility(View.VISIBLE);
                btnChangeYear.setVisibility(View.GONE);
                mAverageBloodFlow.setText(getResources().getString(R.string.averageBloodFlow));
                mMinBloodFlow.setText(getResources().getString(R.string.minBloodFlow));
                mFakeImage.setImageResource(R.mipmap.diagram_month_2018_11);
                mBloodFlowValue1.setText("743");
                mBloodFlowValue2.setText("703");
                pickerSelectNumber = 0;
            }
        });
        mYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMonth.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTransparentBlack));
                mImageViewMonth.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorTransparentBlack));
                mYear.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                mImageViewYear.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorBlue));
                btnChangeMonth.setVisibility(View.GONE);
                btnChangeYear.setText(getResources().getStringArray(R.array.array_year)[0]);
                btnChangeYear.setVisibility(View.VISIBLE);
                mAverageBloodFlow.setText(getResources().getString(R.string.averageBloodFlowYear));
                mMinBloodFlow.setText(getResources().getString(R.string.minBloodFlow));
                mFakeImage.setImageResource(R.mipmap.diagram_year_2018);
                mBloodFlowValue1.setText("752");
                mBloodFlowValue2.setText("737");
                pickerSelectNumber = 0;
            }
        });

        btnChangeMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show();
            }
        });

        btnChangeYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show2();
            }
        });
        return view;
    }


    public void show() {
        final Dialog d = new Dialog(UploadFragment.this.getContext());
        d.setContentView(R.layout.alertdialog);

        // Dialog 視窗大小
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        Objects.requireNonNull(d.getWindow()).setLayout((int) (dm.widthPixels * 0.5), (int) (dm.heightPixels * 0.5));

        Button b2 = (Button) d.findViewById(R.id.button2);
        TextView alertDialogTitle = (TextView) d.findViewById(R.id.alertTitle);
        alertDialogTitle.setText(getResources().getString(R.string.please_select_month));

        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        final String[] arrayMonthData = getResources().getStringArray(R.array.array_month);
        // Initial text picker
        np.setMinValue(0);
        np.setMaxValue(arrayMonthData.length - 1);
        np.setDisplayedValues(arrayMonthData);
        np.setValue(pickerSelectNumber);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                pickerSelectNumber = i1;
            }
        });


        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FakeDataList data = new FakeDataList();
                mFakeImage.setImageResource(data.fakedatamonth().get(pickerSelectNumber).getImageID());
                mBloodFlowValue1.setText(data.fakedatamonth().get(pickerSelectNumber).getBloodflow());
                mBloodFlowValue2.setText(data.fakedatamonth().get(pickerSelectNumber).getMinbloodflow());
                btnChangeMonth.setText(getResources().getStringArray(R.array.array_month)[pickerSelectNumber]);
                d.dismiss();
            }
        });
        d.show();

    }

    public void show2() {
        final Dialog d = new Dialog(UploadFragment.this.getContext());
        d.setContentView(R.layout.alertdialog);

        // Dialog 視窗大小
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        d.getWindow().setLayout((int) (dm.widthPixels * 0.5), (int) (dm.heightPixels * 0.5));

        Button b2 = (Button) d.findViewById(R.id.button2);
        TextView alertDialogTitle = (TextView) d.findViewById(R.id.alertTitle);
        alertDialogTitle.setText(getResources().getString(R.string.please_select_year));

        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        final String[] arrayYearData = getResources().getStringArray(R.array.array_year);
        // Initial text picker
        np.setMinValue(0);
        np.setMaxValue(arrayYearData.length - 1);
        np.setDisplayedValues(arrayYearData);
        np.setValue(pickerSelectNumber);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                pickerSelectNumber = i1;
            }
        });


        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FakeDataList data = new FakeDataList();
                mFakeImage.setImageResource(data.fakedatayear().get(pickerSelectNumber).getImageID());
                mBloodFlowValue1.setText(data.fakedatayear().get(pickerSelectNumber).getBloodflow());
                mBloodFlowValue2.setText(data.fakedatayear().get(pickerSelectNumber).getMinbloodflow());
                btnChangeYear.setText(getResources().getStringArray(R.array.array_year)[pickerSelectNumber]);
                d.dismiss();
            }
        });
        d.show();
    }
}
