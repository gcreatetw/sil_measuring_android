package com.sil.BFVSensing.ChangeLanguage;

public class Constant {
    public static final String SP_NAME = "setting";
    public static final String SP_USER_LANG = "user_language";
}