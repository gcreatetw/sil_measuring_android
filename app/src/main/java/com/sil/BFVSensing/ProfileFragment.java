package com.sil.BFVSensing;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sil.BFVSensing.ChangeLanguage.Constant;
import com.sil.BFVSensing.ChangeLanguage.LangUtils;
import com.sil.BFVSensing.ChangeLanguage.SPUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

/**
 * This class represent profile page
 */
public class ProfileFragment extends Fragment {
    private static final String TAG = "SiL.Profile";
    private EditText mName;
    private EditText mAge;
    private EditText mHeight;
    private EditText mWeight;
    private Spinner mGender;
    //    private EditText mSystolic;
    private EditText mDBP, mSBP;

    // TCP related variables
    private MeasureFragment mMeasure;
    public Socket clientSocket; //客戶端的socket
    private RadioButton mConnect;
    private RadioButton mDisconnect;
    public static String tmp;                // 暫存文字訊息
    private BufferedWriter bw;  //取得網路輸出串流
    private BufferedReader br;  //取得網路輸入串流
    //    private TextView mTcpServerInput;
    //    private EditText mTcpServerInput;
    private Boolean isTCPConnect = false;
    public TextView serverResponse, tcpStatusTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_profile, container, false);
        mName = (EditText) view.findViewById(R.id.profile_name);
        mAge = (EditText) view.findViewById(R.id.profile_age);
        mHeight = (EditText) view.findViewById(R.id.profile_height);
        mWeight = (EditText) view.findViewById(R.id.profile_weight);
        mGender = (Spinner) view.findViewById(R.id.profile_gender);

//        final String[] item = {"Male", "Female"};
        ArrayAdapter<String> itemlist = new ArrayAdapter<>(getActivity(), R.layout.myspinner, getResources().getStringArray(R.array.array_gender));

        mGender.setAdapter(itemlist);
        mGender.setDropDownVerticalOffset(70);

//        mSystolic = (EditText) view.findViewById(R.id.tcp_status);
        mDBP = (EditText) view.findViewById(R.id.profile_DBP);
        mSBP = (EditText) view.findViewById(R.id.profile_SBP);

        mConnect = (RadioButton) view.findViewById(R.id.rbtn_connect);
        mConnect.setOnClickListener(tcpBtnConnect);
        mDisconnect = (RadioButton) view.findViewById(R.id.rbtn_disconnect);
        mDisconnect.setOnClickListener(tcpBtnDisconnect);
//        mTcpServerInput = (EditText) view.findViewById(R.id.tcp_status);
        serverResponse = (TextView) getActivity().findViewById(R.id.measurementStatus);
        tcpStatusTextView = (TextView) getActivity().findViewById(R.id.tcp_connection_status);

        return view;
    }

    private void changeLanguage(int language) {
        //将选择的language保存到SP中
        SPUtils.getInstance(Constant.SP_NAME).put(Constant.SP_USER_LANG, language);
        //重新启动Activity,并且要清空栈
        Intent intent = new Intent(getActivity(), Main.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private View.OnClickListener tcpBtnConnect = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            mConnect.setTextColor(getResources().getColor(R.color.colorBlue));
            mDisconnect.setTextColor(getResources().getColor(R.color.colorDarkGray));
            Toast.makeText(getActivity(), getResources().getString(R.string.server_connecting), Toast.LENGTH_SHORT).show();
            try {
                Thread nctuConnection = new Thread(tcpConnection);
                nctuConnection.start();
            } catch (Exception e) {
            }
        }
    };

    private View.OnClickListener tcpBtnDisconnect = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            mConnect.setTextColor(getResources().getColor(R.color.colorDarkGray));
            mDisconnect.setTextColor(getResources().getColor(R.color.colorBlue));
            try {
                clientSocket.close();
                isTCPConnect = false;
                Thread updateThread = new Thread(updateTcpStatus);
                updateThread.start();
            } catch (Exception e) {
            }
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("profile_name", mName.getText().toString());
        outState.putString("profile_age", mAge.getText().toString());
        outState.putString("profile_height", mHeight.getText().toString());
        outState.putString("profile_weight", mWeight.getText().toString());
        outState.putInt("profile_gender", mGender.getSelectedItemPosition());
//        outState.putString("profile_systolic", mSystolic.getText().toString());
        outState.putString("profile_DBP", mDBP.getText().toString());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) return;
        mName.setText(savedInstanceState.getString("profile_name"));
        mAge.setText(savedInstanceState.getString("profile_age"));
        mHeight.setText(savedInstanceState.getString("profile_height"));
        mWeight.setText(savedInstanceState.getString("profile_weight"));
        mGender.setSelection(savedInstanceState.getInt("profile_gender"));
//        mSystolic.setText(savedInstanceState.getString("profile_systolic"));
//        mSystolic.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                try {
//                    DataManager.getInstance().setCalibrationSBP(Integer.parseInt(s.toString()));
//                } catch (Exception e) {
//                    Log.e(TAG, e.toString());
//                }
//            }
//        });
        mDBP.setText(savedInstanceState.getString("profile_DBP"));
        mDBP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    DataManager.getInstance().setCalibrationDBP(Integer.parseInt(s.toString()));
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

    // 顯示更新訊息
    private Runnable updateTcpStatus = new Runnable() {
        public void run() {
            if (isTCPConnect) {
//                mTcpServerInput.setText("Connected");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tcpStatusTextView.setText(getResources().getString(R.string.connected));
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), getResources().getString(R.string.server_Connected), Toast.LENGTH_SHORT).show();
                            }
                        }, 2000);
                    }
                });
            } else {
//                mTcpServerInput.setText("Disconnect");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tcpStatusTextView.setText(getResources().getString(R.string.disconnect));
                        mDisconnect.setChecked(true);
                        mConnect.setChecked(false);
                        mConnect.setTextColor(getResources().getColor(R.color.colorDarkGray));
                        mDisconnect.setTextColor(getResources().getColor(R.color.colorBlue));
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_Disconnect), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };


    private Runnable tcpConnection = new Runnable() {
        @Override
        public void run() {
            try {
                String nctuServer = "140.113.149.218";
                String gcreate = "192.168.0.180";
                InetAddress serverIp = InetAddress.getByName(nctuServer);
                int serverPort = 5566;
                clientSocket = new Socket(serverIp, serverPort);

                //取得網路輸入串流
                br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                if (clientSocket.isConnected()) {
                    isTCPConnect = true;
                    Thread updateThread = new Thread(updateTcpStatus);
                    updateThread.start();
                }

                Thread a = new Thread(new Runnable() {
                    public void run() {
                        BufferedWriter bw;
                        try {
                            // 取得網路輸出串流
                            bw = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
                            // 寫入訊息
                            String SBP = mSBP.getText().toString();
                            String DBP = mDBP.getText().toString();
                            if (!TextUtils.isEmpty(SBP) && !TextUtils.isEmpty(DBP)) {
                                ArrayList<String> list = new ArrayList<String>();
                                list.add("[SBP]" + mSBP.getText().toString() + "[SBP]");
                                list.add("[DBP]" + mDBP.getText().toString() + "[DBP]");
                                for (int i = 0; i < 2; i++) {
                                    bw.write(list.get(i));
                                    bw.flush();
                                }
                            }
                        } catch (IOException e) {
                        }
                    }
                });
                a.start();

                //檢查是否已連線
                while (clientSocket.isConnected()) {
                    //宣告一個緩衝,從br串流讀取 Server 端傳來的訊息
                    tmp = br.readLine();
                    if (tmp != null) {
                        Log.d("//", "=======================================");
                        Log.d("//", tmp);
                        Log.d("//", "=======================================");
                        //mHandler.post(updateTcpStatus);

                        serverResponse.post(new Runnable() {
                            @Override
                            public void run() {
                                serverResponse.setText(tmp);
                            }
                        });
                    }
                }
            } catch (Exception e) {
                //當斷線時會跳到 catch,可以在這裡處理斷開連線後的邏輯
                try {
                    e.printStackTrace();
                    Log.e("text", "Socket連線=" + e.toString());
                    isTCPConnect = false;
                    Thread updateThread = new Thread(updateTcpStatus);
                    updateThread.start();
                    br.close();
                    clientSocket.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    };
}
