package com.sil.BFVSensing;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This class represent cloud page
 */
public class CloudFragment extends Fragment {
    private static final String TAG = "SiL.Cloud";
    /* Indicate real-time graph of heart rate */
    GraphView mHrGraph;
    /* Indicate the data for graph */
    private LineGraphSeries<DataPoint> mHrSeries;
    private ArrayList<Integer> mHr = new ArrayList<>();

    /* Indicate real-time graph of SBP */
    GraphView mSbpGraph;
    /* Indicate the data for graph */
    private LineGraphSeries<DataPoint> mSbpSeries;
    private ArrayList<Integer> mSbp = new ArrayList<>();

    /* Indicate real-time graph of DBP */
    GraphView mDbpGraph;
    /* Indicate the data for graph */
    private LineGraphSeries<DataPoint> mDbpSeries;
    private ArrayList<Integer> mDbp = new ArrayList<>();

    /* Upload button */
    private Button mUpload;
    private RequestQueue mQueue;
    private RequestQueue.RequestFinishedListener mFinishListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_cloud, container, false);
        mHrGraph = (GraphView) view.findViewById(R.id.graph_hr);
        mSbpGraph = (GraphView) view.findViewById(R.id.graph_sbp);
        mDbpGraph = (GraphView) view.findViewById(R.id.graph_dbp);
        mUpload = (Button) view.findViewById(R.id.upload);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntegerArrayList("hr", mHr);
        outState.putIntegerArrayList("sbp", mSbp);
        outState.putIntegerArrayList("dbp", mDbp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initGraph();
        mUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UploadAsynTask().execute();
            }
        });
        mQueue = Volley.newRequestQueue(getActivity());
        mFinishListener = new RequestQueue.RequestFinishedListener() {
            @Override
            public void onRequestFinished(Request request) {
                synchronized (mQueue) {
                    Log.d(TAG, "onRequestFinished --> notify");
                    mQueue.notify();
                }
            }
        };
        mQueue.addRequestFinishedListener(mFinishListener);
        if (savedInstanceState == null) {
            restoreState(getArguments());
            return;
        }
        mHr = savedInstanceState.getIntegerArrayList("hr");
        mSbp = savedInstanceState.getIntegerArrayList("sbp");
        mDbp = savedInstanceState.getIntegerArrayList("dbp");
        for (int i = 0; i < mHr.size(); i++) {
            mHrSeries.appendData(new DataPoint(i, mHr.get(i)), true, 200);
        }
        for (int i = 0; i < mSbp.size(); i++) {
            mSbpSeries.appendData(new DataPoint(i, mSbp.get(i)), true, 200);
        }
        for (int i = 0; i < mDbp.size(); i++) {
            mDbpSeries.appendData(new DataPoint(i, mDbp.get(i)), true, 200);
        }
    }

    private void initGraph() {
        if (mHrGraph == null || mSbpGraph == null || mDbpGraph == null) {
            Log.e(TAG, "initSeries fail!");
            return;
        }
        /* mHrGraph */
        mHrGraph.removeAllSeries();
        mHrSeries = new LineGraphSeries<>();
        mHrGraph.addSeries(mHrSeries);
        mHrGraph.getViewport().setXAxisBoundsManual(true);
        mHrGraph.getViewport().setYAxisBoundsManual(true);
        mHrGraph.getViewport().setMinX(0);
        mHrGraph.getViewport().setMaxX(100);
        mHrGraph.getViewport().setMinY(0);
        mHrGraph.getViewport().setMaxY(300);

        /* mSbpGraph */
        mSbpGraph.removeAllSeries();
        mSbpSeries = new LineGraphSeries<>();
        mSbpGraph.addSeries(mSbpSeries);
        mSbpGraph.getViewport().setXAxisBoundsManual(true);
        mSbpGraph.getViewport().setYAxisBoundsManual(true);
        mSbpGraph.getViewport().setMinX(0);
        mSbpGraph.getViewport().setMaxX(100);
        mSbpGraph.getViewport().setMinY(0);
        mSbpGraph.getViewport().setMaxY(300);

        /* mHrGraph */
        mDbpGraph.removeAllSeries();
        mDbpSeries = new LineGraphSeries<>();
        mDbpGraph.addSeries(mDbpSeries);
        mDbpGraph.getViewport().setXAxisBoundsManual(true);
        mDbpGraph.getViewport().setYAxisBoundsManual(true);
        mDbpGraph.getViewport().setMinX(0);
        mDbpGraph.getViewport().setMaxX(100);
        mDbpGraph.getViewport().setMinY(0);
        mDbpGraph.getViewport().setMaxY(300);
    }

    public void notifyFinishMeasure() {
        DataManager manager = DataManager.getInstance();
        mHr.add(manager.getHR());
        mHrSeries.appendData(new DataPoint(mHr.size(), manager.getHR()), false, 100);
        mSbp.add(manager.getSBP());
        mSbpSeries.appendData(new DataPoint(mSbp.size(), manager.getSBP()), false, 100);
        mDbp.add(manager.getDBP());
        mDbpSeries.appendData(new DataPoint(mDbp.size(), manager.getDBP()), false, 100);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        saveState();
    }

//    private Bundle saveState() {
//        Bundle state = new Bundle();
//        state.putIntegerArrayList("hr", mHr);
//        state.putIntegerArrayList("sbp", mSbp);
//        state.putIntegerArrayList("dbp", mDbp);
//        setArguments(state);
//        return state;
//    }

    private void restoreState(Bundle savedInstanceState) {
        if (savedInstanceState == null) return;
        mHr = savedInstanceState.getIntegerArrayList("hr");
        mSbp = savedInstanceState.getIntegerArrayList("sbp");
        mDbp = savedInstanceState.getIntegerArrayList("dbp");
        for (int i = 0; i < mHr.size(); i++) {
            mHrSeries.appendData(new DataPoint(i, mHr.get(i)), false, 200);
        }
        for (int i = 0; i < mSbp.size(); i++) {
            mSbpSeries.appendData(new DataPoint(i, mSbp.get(i)), false, 200);
        }
        for (int i = 0; i < mDbp.size(); i++) {
            mDbpSeries.appendData(new DataPoint(i, mDbp.get(i)), false, 200);
        }
    }

    private void resetData() {
        mHr.clear();
        mSbp.clear();
        mDbp.clear();
        initGraph();
        Toast.makeText(getActivity(), "Upload finished", Toast.LENGTH_LONG).show();
    }

    private class UploadAsynTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i < mHr.size(); i++) {
                synchronized (mQueue) {
                    mQueue.add(buildPOSTData(mHr.get(i), mSbp.get(i), mDbp.get(i)));
                    Log.d(TAG, "add --> " + mHr.get(i) + " " + mSbp.get(i) + " " + mDbp.get(i));
                    try {
                        mQueue.wait();
                        Thread.sleep(2000);
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                }
            }
            resetData();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            resetData();
            super.onPostExecute(aVoid);
        }
    }

    private JsonRequest buildPOSTData(int hr, int sbp, int dbp) {
        String url = "https://api.mediatek.com/mcs/v2/devices/DY3yq10H/datapoints";
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonarray = new JSONArray();
        try {
            jsonarray.put(createDataPoint(hr, "mHR"));
            jsonarray.put(createDataPoint(sbp, "mSBP"));
            jsonarray.put(createDataPoint(dbp, "mDBP"));
            jsonObject.put("datapoints", jsonarray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JsonRequest<JSONObject> jsonRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "response -> " + response.toString());

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.getMessage(), error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("deviceKey", "H9mYKJ3sLYqU8eC9");
                headers.put("Content-Type", "application/json");

                return headers;
            }
        };
        return jsonRequest;
    }

    private JSONObject createDataPoint(int data, String channelId) {
        JSONObject jsonObjectValue = new JSONObject();
        JSONObject jsonObjectDataPoint = new JSONObject();
        try {
            jsonObjectValue.put("value", data);
            jsonObjectDataPoint.put("dataChnId", channelId);
            jsonObjectDataPoint.put("values", jsonObjectValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObjectDataPoint;
    }

    @Override
    public void onStop() {
        super.onStop();
        mQueue.removeRequestFinishedListener(mFinishListener);
    }
}
