package com.sil.BFVSensing;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
//import android.view.Menu;
//import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Measuring extends Activity {
    private static final String TAG = "SiL.Measuring";
    private static final int ANIMATION_INTERVAL = 100;
    /* Message in circle progressbar */
    private TextView mTextInProgressBar;
    /* Instance of progressbar */
    private ProgressBar mCircleProgressBar;

    /* Handler for animate progress */
    private Handler mHandler = new Handler();
    /* Task for update animation status */
    private Runnable mUpdateProgress = new Runnable() {
        @Override
        public void run() {
            if (!mIsBTReadyForUse) {
                resetProgress();
                showBtIsNotReady();
                return;
            }

            if (mCurrentProgress < 10) {
                mHandler.postDelayed(this, ANIMATION_INTERVAL);
                mTextInProgressBar.setText(Integer.toString(mCurrentProgress));
                mCircleProgressBar.setProgress(mCurrentProgress);
                mCurrentProgress++;
            } else if (mCurrentProgress == 10) {
                if (mContinuousMeasure) {
                    mCircleProgressBar.setProgress(10);
                    mTextInProgressBar.setText(Integer.toString(mCurrentProgress));
                    mHandler.postDelayed(this, ANIMATION_INTERVAL);
                    mCurrentProgress = 1;
                } else {
                    resetProgress();
                }
            }
        }
    };
    /* Current animate value */
    private int mCurrentProgress = 1;
    /* This flag indicates whether we do infinite measurement or not */
    private boolean mContinuousMeasure = false;
    /* This flag indicates whether measurement is on the way */
    private boolean mIsMeasuring = false;

    //-----------------------------------------
    //     Initialize BT related instance
    //-----------------------------------------
    /* Represents the local device Bluetooth adapter */
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    /* This flag indicates whether BT is ready for use */
    private boolean mIsBTReadyForUse = false;
    /* Monitor BT related changes */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        mIsBTReadyForUse = false;
                        break;
                    case BluetoothAdapter.STATE_ON:
                    case BluetoothAdapter.STATE_TURNING_ON:
                        mIsBTReadyForUse = true;
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        Log.d("Puwu", "onCreate");
        setContentView(R.layout.activity_measuring);

//        /* Configure actionbar */
//        ActionBar actionBar = getActionBar();
//        actionBar.setBackgroundDrawable(new ColorDrawable(0xFFEE1267));
//        actionBar.setTitle(Html.fromHtml("<font color='#ffffff'>BFV Sensing</font>"));

        /* Configure status bar */
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(0xF2E8CF7);

        /* Initialize all views */
        mTextInProgressBar = (TextView) findViewById(R.id.textInProgressBar);
        mTextInProgressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsMeasuring) return;
                if (!mIsBTReadyForUse) {
                    showBtIsNotReady();
                    return;
                }

                mTextInProgressBar.setTextColor(Color.parseColor("#EE1267"));
                mTextInProgressBar.setText("0");
                mTextInProgressBar.setBackgroundResource(R.drawable.innercirclestyle_inprogress);
                mTextInProgressBar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 60);
                mTextInProgressBar.setTypeface(null, Typeface.BOLD);
                mHandler.postDelayed(mUpdateProgress, ANIMATION_INTERVAL);
                mIsMeasuring = true;
            }
        });
        mCircleProgressBar = (ProgressBar) findViewById(R.id.circularProgressBar);

        /* Initialize Bluetooth */
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        this.registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        // 1. Check BT status: if BT is not ready, prompt a dialog to hint user
        mIsBTReadyForUse = mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
        if (!mIsBTReadyForUse) {
            new AlertDialog.Builder(Measuring.this)
                    .setTitle("Bluetooth is not ready")
                    .setMessage("Please go to Settings and check BT status. Make sure BT is enabled"
                            + " and device has ever connected with blood measurement sensor!")
                    .setPositiveButton("OK", null)
                    .setIcon(R.drawable.warning)
                    .show();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_measuring, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_set_infinite) {
//            mContinuousMeasure = !item.isChecked();
//            item.setChecked(!item.isChecked());
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    private void resetProgress() {
        mCircleProgressBar.setProgress(0);
        mTextInProgressBar.setText("Start");
        mTextInProgressBar.setTextColor(Color.parseColor("#FFFFFF"));
        mTextInProgressBar.setBackgroundResource(R.drawable.innercirclestyle_initial);
        mTextInProgressBar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
        mTextInProgressBar.setTypeface(null, Typeface.NORMAL);
        mCurrentProgress = 1;
        mIsMeasuring = false;
    }

    private void showBtIsNotReady() {
        Toast.makeText(Measuring.this, "BT is not ready", Toast.LENGTH_LONG).show();
    }
}
