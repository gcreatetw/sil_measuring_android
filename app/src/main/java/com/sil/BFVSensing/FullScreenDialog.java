package com.sil.BFVSensing;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

/**
 * Created by Frank on 2018/5/8.
 */
public class FullScreenDialog extends Dialog {

    private Context mContext;
    private Handler mHandler = new Handler();
    private Runnable mCountdown = new Runnable() {
        @Override
        public void run() {
            if (mCurrentIndex == 1) {
                if (FullScreenDialog.this.isShowing()) {
                    FullScreenDialog.this.dismiss();
                    return;
                }
            }
            mSwitcher.setText(Integer.toString(--mCurrentIndex));
            mHandler.postDelayed(mCountdown, 1000);
        }
    };
    private TextSwitcher mSwitcher;
    private int mCurrentIndex = 0;

    public FullScreenDialog(Context context) {
        super(context);
        mContext = context;
        mSwitcher = new TextSwitcher(context);
        mSwitcher.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        mCurrentIndex = 3;
        mSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView textView = new TextView(mContext);
                textView.setGravity(Gravity.CENTER);
                textView.setTextSize(192);
                textView.setTextColor(Color.parseColor("#FFFFFF"));
                return textView;
            }
        });
        mHandler.postDelayed(mCountdown, 1000);
        mSwitcher.setCurrentText("3");
        mSwitcher.setInAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_up_animation));
        mSwitcher.setOutAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_down_animation));
        addContentView(mSwitcher, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }
}