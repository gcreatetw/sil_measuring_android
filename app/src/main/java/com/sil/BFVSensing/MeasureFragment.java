package com.sil.BFVSensing;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

/**
 * This class represent measuring page
 */
public class MeasureFragment extends Fragment {
    private static final String TAG = "SiL.Measuring";
    private static final int ANIMATION_INTERVAL = 100;
    /* Message in circle progressbar */
    private TextView mTextInProgressBar;
    /* Instance of progressbar */
    private ProgressBar mCircleProgressBar;
    TextView mBTConnectionStatus;
    /* Measurement result and status */
    TextView mMeasurementStatus;
    /* Indicate real-time graph */
    GraphView mGraph;
    /* Indicate the data for graph */
    private LineGraphSeries<DataPoint> mSeries;

    /* Handler for animate progress */
    private Handler mHandler = new Handler();
    /* Task for update animation status */
    private Runnable mUpdateProgress = new Runnable() {
        @Override
        public void run() {
            if (!mIsBTReadyForUse) {
                resetProgress();
                showBtIsNotReady("BT is not ready");
                return;
            }

            if (mCurrentProgress < 300) {
                mHandler.postDelayed(this, ANIMATION_INTERVAL);
                //mTextInProgressBar.setText(Integer.toString(mCurrentProgress));
                mTextInProgressBar.setText(Integer.toString(mCurrentProgress / 10));
                mCircleProgressBar.setProgress(mCurrentProgress / 3);
                mCurrentProgress++;
            } else if (mCurrentProgress == 300) {
                if (mContinuousMeasure) {
                    mCircleProgressBar.setProgress(100);
                    mTextInProgressBar.setText(Integer.toString(mCurrentProgress / 10));
                    mHandler.postDelayed(this, ANIMATION_INTERVAL);
                    mCurrentProgress = 0;
                    if (mMeasureCallback != null) mMeasureCallback.onCalculate();
                } else {
                    resetProgress();
                    if (mMeasureCallback != null) {
                        mMeasureCallback.onStop();
                        mMeasureCallback.onCalculate();
                    }
                }
            }
        }
    };
    /* Current animate value */
    private int mCurrentProgress = 1;
    /* This flag indicates whether we do infinite measurement or not */
    private boolean mContinuousMeasure = false;
    /* This flag indicates whether measurement is on the way */
    private boolean mIsMeasuring = false;

    //-----------------------------------------
    //     Initialize BT related instance
    //-----------------------------------------
    /* Represents the local device Bluetooth adapter */
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    /* This flag indicates whether BT is ready for use */
    private boolean mIsBTReadyForUse = false;
    /* Monitor BT related changes */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        mIsBTReadyForUse = false;
                        break;
                    case BluetoothAdapter.STATE_ON:
                    case BluetoothAdapter.STATE_TURNING_ON:
                        mIsBTReadyForUse = true;
                        break;
                }
            }
        }
    };
    /* Callback function to notify start measuring */
    MeasureCallback mMeasureCallback = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_measuring, container, false);

        /* Initialize all views */
        mTextInProgressBar = (TextView) view.findViewById(R.id.textInProgressBar);
        mTextInProgressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mIsBTReadyForUse) {
                    showBtIsNotReady(getResources().getString(R.string.showBtIsNotReadyMessage));
                    return;
                } else if (!((Main) getActivity()).isConnected()) {
                    showBtIsNotReady(getResources().getString(R.string.showBtIsNotReadyMessage));
                    return;
                }
                if (mIsMeasuring) {
                    resetProgress();
                    if (mMeasureCallback != null) {
                        mHandler.removeCallbacks(mUpdateProgress);
                        mMeasureCallback.onStop();
                        mMeasureCallback.onForceStop();
                    }
                    return;
                }
                FullScreenDialog dialog = new FullScreenDialog(getActivity());
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        mTextInProgressBar.setTextColor(Color.parseColor("#2E8CF7"));
                        mTextInProgressBar.setText("0");
                        mTextInProgressBar.setBackgroundResource(R.drawable.innercirclestyle_inprogress);
                        mTextInProgressBar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 60);
                        mTextInProgressBar.setTypeface(null, Typeface.BOLD);
                        mHandler.postDelayed(mUpdateProgress, ANIMATION_INTERVAL);
                        mIsMeasuring = true;
                        if (mMeasureCallback != null) {
                            mGraph.removeAllSeries();
                            mSeries = new LineGraphSeries<>();
                            mGraph.addSeries(mSeries);
                            mMeasureCallback.onStart();
                        }
                    }
                });
                dialog.show();
            }
        });
        mCircleProgressBar = (ProgressBar) view.findViewById(R.id.circularProgressBar);
        mBTConnectionStatus = (TextView) view.findViewById(R.id.bt_connection_status);
        mMeasurementStatus = (TextView) view.findViewById(R.id.measurementStatus);
        mGraph = (GraphView) view.findViewById(R.id.graph);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        // 1. Check BT status: if BT is not ready, prompt a dialog to hint user
        mIsBTReadyForUse = mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
        if (!mIsBTReadyForUse) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(getResources().getString(R.string.bluetooth_isOpen))
                    .setMessage(getResources().getString(R.string.bluetooth_isOpen_description))
                    .setPositiveButton(getResources().getString(R.string.ok), null)
                    .setIcon(R.drawable.warning)
                    .show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /* Initialize Bluetooth */
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        getActivity().registerReceiver(mReceiver, filter);
        initGraph();
    }

    /*
     * Set measuring mode to continuous mode or single mode.
     */
    public void setMeasuringMode(boolean isContinuous) {
        mContinuousMeasure = isContinuous;
    }

    /*
     * Reset progress bar to default
     */
    private void resetProgress() {
        mCircleProgressBar.setProgress(0);
        mTextInProgressBar.setText(getResources().getString(R.string.start));
        mTextInProgressBar.setTextColor(Color.parseColor("#FFFFFF"));
        mTextInProgressBar.setBackgroundResource(R.drawable.innercirclestyle_initial);
        mTextInProgressBar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
        mTextInProgressBar.setTypeface(null, Typeface.NORMAL);
        mCurrentProgress = 1;
        mIsMeasuring = false;
    }

    private void showBtIsNotReady(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    /**
     * Callback for notifying start to measure
     */
    public interface MeasureCallback {
        public void onStart();

        public void onStop();

        public void onCalculate();

        public void onForceStop();
    }

    public void setMeasureCallback(MeasureCallback callback) {
        mMeasureCallback = callback;
    }

    public void notifyDisconnect() {
        mHandler.removeCallbacks(mUpdateProgress);
        mMeasureCallback.onStop();
        mMeasureCallback.onForceStop();
    }

    private void initGraph() {
        if (mGraph == null) {
            Log.e(TAG, "initSeries fail!");
            return;
        }
        mGraph.removeAllSeries();
        mSeries = new LineGraphSeries<>();
        mGraph.addSeries(mSeries);
        mGraph.getViewport().setXAxisBoundsManual(true);
        mGraph.getViewport().setYAxisBoundsManual(false);
        mGraph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        mGraph.getGridLabelRenderer().setHorizontalAxisTitle(getResources().getString(R.string.time));
        mGraph.getViewport().setMinX(0);
        mGraph.getViewport().setMaxX(1000);
        mGraph.getViewport().setMinY(0);
        mGraph.getViewport().setMaxY(100000);
    }

    public void notifyData(DataPoint data) {
        mSeries.appendData(data, true, 1000);
    }

    public void showRealtimeData(boolean show) {
        if (show) {
            mGraph.setVisibility(View.VISIBLE);
        } else {
            mGraph.setVisibility(View.INVISIBLE);
        }
    }
}
