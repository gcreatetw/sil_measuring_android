package com.sil.BFVSensing;

import android.util.Log;

import java.util.ArrayList;

/**
 * This class calculate heart rate, SBP, and DBP
 */
public class DataManager {
    private static final String TAG = "SiL.DataManager";
    // Retrieve every 10s data
    private static final int WINDOW_WIDTH = 10;
    private static final int SAMPLE_FREQUENCY = 500;
    private static final int MAX_HR = 2;
    private static final int REGISTER = 100;
    private static final int DEFAULT_MEM_SIZE = WINDOW_WIDTH * SAMPLE_FREQUENCY;
    private int mSize = WINDOW_WIDTH * SAMPLE_FREQUENCY;
    private int mCalibrationSBP = 118;
    private int mCalibrationDBP = 81;
    private int mHR = 0;
    private int mSBP = 0;
    private int mDBP = 0;
    // A set of data from sensor
    private ArrayList<Integer> mData = new ArrayList<>();
    private static DataManager sDataManager = null;

    private DataManager() {
        /* hide constructor */
    }

    public static DataManager getInstance() {
        if (sDataManager == null) {
            sDataManager = new DataManager();
        }
        return sDataManager;
    }

    public void calculateAverage() {
        Log.d(TAG, "===== Calculate =====");
        Log.d(TAG, "mData size = " + mData.size());
        if (mData.size() < mSize) mSize = mData.size();
        float[] firstFilterResult = firstFilter();
        float[] secondFilterResult = secondFilter(firstFilterResult);
        scanForMaxMinPeaks(secondFilterResult);
        mSize = DEFAULT_MEM_SIZE;
        Log.d(TAG, "HR = " + mHR + " mSBP = " + mSBP + " mDBP = " + mDBP);
    }

    public int getHR() {
        return mHR;
    }

    public int getSBP() {
        return mSBP;
    }

    public int getDBP() {
        return mDBP;
    }

    public void clear() {
        int mHR = 0;
        int mSBP = 0;
        int mDBP = 0;
        mData.clear();
    }

    private float[] firstFilter() {
        //=================================================================
        // First Filter initial
        //=================================================================
        float firstFilterSum = 0;
        float firstNowFilterVal = 0;
        float[] firstLastFilterVal = new float[mSize];
        int i = 0;
        int j = 0;
        int firstFilterIdx = 0;
        int firstFilterN = 40;
        //=================================================================
        // First Filter
        //=================================================================
        for (firstFilterIdx = firstFilterN + 1; firstFilterIdx < mSize; firstFilterIdx++) {
            // sum the original signal for average
            for (i = firstFilterIdx - firstFilterN; i <= firstFilterIdx; i++) {
                firstFilterSum = firstFilterSum + mData.get(i);
            }
            // average the sum of the original signal
            firstNowFilterVal = firstFilterSum / firstFilterN;
            // save the first filter processed signal in the array
            firstLastFilterVal[j] = firstNowFilterVal;
            j = j + 1;
            // reset the sum variable
            firstFilterSum = 0;
        }

        return firstLastFilterVal;
    }

    private float[] secondFilter(float[] firstLastFilterVal) {
        //=================================================================
        // Second Filter initial
        //=================================================================
        int secFilterIdx = 0;
        float secFilterSum = 0;
        float secNowFilterVal = 0;
        int secFilterN = 40;
        int i = 0;
        int j = 0;
        float[] mem = new float[mSize];
        //=================================================================
        // Second Filter initial
        //=================================================================
        // scan the first filter processed signal
        for (secFilterIdx = secFilterN + 1; secFilterIdx < mSize; secFilterIdx++) {
            // sum the original signal for average
            for (i = secFilterIdx - secFilterN; i < secFilterIdx; i++) {
                secFilterSum = secFilterSum + firstLastFilterVal[i];
            }
            // average the sum of the original signal
            secNowFilterVal = secFilterSum / secFilterN;
            // save the first filter processed signal in the array
            mem[j] = secNowFilterVal;
            j = j + 1;
            // reset the sum variable
            secFilterSum = 0;
        }
        return mem;
    }

    private void scanForMaxMinPeaks(float[] mem) {
        int scanIndex = 0;
        int maxRegIdx = 0;
        int minRegIdx = 0;
        int nowMaxIdx = 0;
        int nowMinIdx = 0;
        int lastMaxIdx = 0;
        int lastMinIdx = 0;
        int minPeakDist = SAMPLE_FREQUENCY / MAX_HR;
        int count = 0;
        float nowMaxVal = 0;
        float nowMinVal = 0;
        float lastMaxVal = 0;
        float lastMinVal = 0;
        float[] maxPeaksValAll = new float[REGISTER];
        int[] maxPeaksIdxAll = new int[REGISTER];
        float[] minPeaksValAll = new float[REGISTER];
        int[] minPeaksIdxAll = new int[REGISTER];

        for (scanIndex = 10; scanIndex < mSize; scanIndex++) {
            //-----------------------
            // skip some head and tail to prevent bug
            //-----------------------
            if (scanIndex < 10 || scanIndex > mSize - 10) {
                continue;
            }
            //------------------------------------------------------------
            // max
            //------------------------------------------------------------
            // scan for first maximum peaks
            if (mem[scanIndex] >= mem[scanIndex - 1]
                    && mem[scanIndex] >= mem[scanIndex - 2]
                    && mem[scanIndex] >= mem[scanIndex + 1]
                    && mem[scanIndex] >= mem[scanIndex + 2]) {
                // save the value
                nowMaxVal = mem[scanIndex];
                nowMaxIdx = scanIndex;
                // compare to last value
                if (nowMaxIdx - lastMaxIdx < minPeakDist && nowMaxVal > lastMaxVal) {
                    // within a pulse and greater than last, than new maximum peak is found
                    lastMaxVal = nowMaxVal;
                    lastMaxIdx = nowMaxIdx;
                }
            }
            // exceed a pulse and no other maximum is found, record the exact peaks
            if (scanIndex - lastMaxIdx > minPeakDist) {
                // record
                maxPeaksValAll[maxRegIdx] = lastMaxVal;
                maxPeaksIdxAll[maxRegIdx] = lastMaxIdx;
                maxRegIdx++;
                // reset the last peaks to current scanned point
                lastMaxVal = mem[scanIndex];
                lastMaxIdx = scanIndex;
            }

            //------------------------------------------------------------
            // min
            //------------------------------------------------------------
            // scan for minimum peaks
            if (mem[scanIndex] <= mem[scanIndex - 1]
                    && mem[scanIndex] <= mem[scanIndex - 2]
                    && mem[scanIndex] <= mem[scanIndex + 1]
                    && mem[scanIndex] <= mem[scanIndex + 2]) {
                // save the value
                nowMinVal = mem[scanIndex];
                nowMinIdx = scanIndex;
                // compare to last value
                if (nowMinIdx - lastMinIdx < minPeakDist && nowMinVal < lastMinVal) {
                    // within a pulse and smaller than last, than new minimun peak is found
                    lastMinVal = nowMinVal;
                    lastMinIdx = nowMinIdx;
                }
            }
            // exceed a pulse and no other minimun is found, record the exact peaks
            if (scanIndex - lastMinIdx > minPeakDist) {
                // record
                minPeaksValAll[minRegIdx] = lastMinVal;
                minPeaksIdxAll[minRegIdx] = lastMinIdx;
                // save the probability Available pair
                count = minRegIdx;
                minRegIdx++;
                // reset the last peaks to current scanned point
                lastMinVal = mem[scanIndex];
                lastMinIdx = scanIndex;
            }
        }

        int i = 0;
        int j = 0;
        int sum = 0;
        nowMaxIdx = 0;
        nowMaxVal = 0;
        int nextMaxIdx = 0;
        float nextMaxVal = 0;
        minRegIdx = 0;
        int[] selected = new int[REGISTER];
        int numberFound = 1; // start with 1 means save the pair in the first column
        int[][] maxMinPair = new int[2][REGISTER];
        float[] secPeaksValAll = new float[REGISTER];
        int[] secPeaksIdxAll = new int[REGISTER];
        int[] secPeaksPpdAll = new int[REGISTER];
        int startIdx = 0;
        int endIdx = 0;
        // set and reset the variable
        float v = 0;
        int x = 0;
        int y = 0;
        int z = 0;
        // scan loop for maximum and minimum pair (for secondary peaks later)
        for (i = 0; i < count; i++) {
            for (j = 0; j < 100; j++) {
                selected[j] = 0;
            }
            // skip some head and tail to prevent bug
            if (i == 0 || i == count) {
                continue;
            }
            // extract now max peaks
            nowMaxVal = maxPeaksValAll[i];
            nowMaxIdx = maxPeaksIdxAll[i];
            // extract next max peaks
            nextMaxVal = maxPeaksValAll[i + 1];
            nextMaxIdx = maxPeaksIdxAll[i + 1];
            // find if there is min within two max
            for (minRegIdx = 0; minRegIdx <= count; minRegIdx++) {
                if (minPeaksIdxAll[minRegIdx] > nowMaxIdx && minPeaksIdxAll[minRegIdx] < nextMaxIdx) {
                    selected[minRegIdx] = 1;
                }
            }
            for (j = 0; j < REGISTER; j++) {
                sum = sum + selected[j];
            }

            // skip if there is no availible pair
            if (sum == 0) {
                continue;
            } else {// % extract the selected min peak (the first one appears in selected)
                for (int k = 0; k < REGISTER; k++) {
                    if (selected[k] == 1) {
                        nowMinVal = minPeaksValAll[k];
                        nowMinIdx = minPeaksIdxAll[k];
                        //the first one appears in selected
                        break;
                    }
                }
                // NEW: save the pair of index instead of find the secondary peaks in this loop
                for (; numberFound < REGISTER; ) {
                    maxMinPair[0][numberFound] = nowMaxIdx;
                    maxMinPair[1][numberFound] = nowMinIdx;
                    numberFound++;
                    break;
                }
            }
        }


        ////////////////////////////////////////////////////////////////////////////
        //                          Second Peaks Detection
        ////////////////////////////////////////////////////////////////////////////
        // scan loop for secondary peaks
        for (x = 0; x < numberFound; x++) {
            // extract the max and min pair
            startIdx = maxMinPair[0][x];
            endIdx = maxMinPair[1][x];
            // find the first presented peaks as secondary peaks (need some double checks when I am free)

            for (y = startIdx; y < endIdx; y++) {
                // extract the value
                v = mem[y];
                // skip some head and tail to prevent possible bug
                if (y < startIdx + 12 || y > endIdx - 30) {
                    continue;
                }

                // find if it is peaks, record it, get ppd and break
                if (v >= mem[y - 1]
                        && v >= mem[y - 2]
                        && v >= mem[y - 3]
                        && v > mem[y - 5]
                        && v >= mem[y + 1]
                        && v >= mem[y + 2]
                        && v >= mem[y + 3]
                        && v > mem[y + 5]) {
                    // save the second peak value, index and PPD
                    secPeaksValAll[z] = v;
                    secPeaksIdxAll[z] = y;
                    secPeaksPpdAll[z] = y - startIdx; // unit: number of samples
                    z++;
                    break;
                }
            }
        }

        int diffScanIndex = 0;
        startIdx = 10;
        i = 0;
        float nowFirstDiffVal = 0;
        float[] lastFirstDiffVal = new float[mSize];

        // scan the original signal
        for (diffScanIndex = startIdx; diffScanIndex < mSize; diffScanIndex++) {
            // differential the neighboring point
            nowFirstDiffVal = mem[diffScanIndex] - mem[diffScanIndex - 1];
            // save the value
            lastFirstDiffVal[i] = nowFirstDiffVal;
            i++;
        }
        //----------------------------------------------------------------
        j = 0;
        i = 0;
        int DiffFilterIdx = 0;
        int DiffFilterN = 20;
        float DiffFilterSum = 0;
        float nowDiffFilterVal = 0;
        float[] lastDiffFilterVal = new float[mSize];
        //=================================================================
        // First Differential Filter
        //=================================================================
        for (DiffFilterIdx = DiffFilterN / 2 + 1; DiffFilterIdx < mSize - DiffFilterN / 2 - 1; DiffFilterIdx++) {
            // sum the original signal for average
            for (i = DiffFilterIdx - DiffFilterN / 2; i <= DiffFilterIdx + DiffFilterN / 2; i++) {
                DiffFilterSum = DiffFilterSum + lastFirstDiffVal[i];
            }
            // average the sum of the original signal
            nowDiffFilterVal = DiffFilterSum / (DiffFilterN + 1);
            // save the First Differential Filtered signal in the array
            lastDiffFilterVal[j] = nowDiffFilterVal;
            j = j + 1;
            // reset the sum variable
            DiffFilterSum = 0;
        }
        //----------------------------------------------------------------
        i = 0;
        int DiffIdx = 0;
        float nowSecDiffVal = 0;
        float[] lastSecDiffVal = new float[mSize];
        //=================================================================
        // Second Differential
        //=================================================================
        // scan the signal
        for (DiffIdx = 2; DiffIdx < mSize; DiffIdx++) {
            // differential the neighboring point
            nowSecDiffVal = lastDiffFilterVal[DiffIdx] - lastDiffFilterVal[DiffIdx - 1];

            // save the value
            lastSecDiffVal[i] = nowSecDiffVal;
            i++;
        }
        //----------------------------------------------------------------
        i = 0;
        j = 0;
        int secDiffFilterIdx = 0;
        int secDiffFilterN = 20;
        float secDiffFilterSum = 0;
        float secNowDiffFilterVal = 0;
        float[] secLastDiffFilterVal = new float[mSize];
        //=================================================================
        // Second Differential Filter
        //=================================================================
        for (secDiffFilterIdx = secDiffFilterN / 2 + 1; secDiffFilterIdx < mSize - DiffFilterN / 2 - 1; secDiffFilterIdx++) {
            // sum the signal for average
            for (i = secDiffFilterIdx - secDiffFilterN / 2; i <= secDiffFilterIdx + secDiffFilterN / 2; i++) {
                secDiffFilterSum = secDiffFilterSum + lastSecDiffVal[i];
            }
            // average the sum of the signal
            secNowDiffFilterVal = secDiffFilterSum / (secDiffFilterN + 1);
            // save the Second Differential Filtered signal in the array
            secLastDiffFilterVal[j] = secNowDiffFilterVal;
            j++;
            // reset the sum variable
            secDiffFilterSum = 0;
        }
        x = 0;
        y = 0;
        z = 0;
        v = 0;
        float w = 0;
        float[] diffPeaksValAll = new float[mSize];
        int[] diffPeaksIdxAll = new int[mSize];
        float[] diffPeaksPpdAll = new float[mSize];

        //========================================================================================================
        // Scan the maxMinPair to detection the point which transforms from "POSITIVE" to "NEGATIVE"
        //========================================================================================================
        // scan loop for secondary peaks
        for (x = 0; x < numberFound; x++) {
            // extract the max and min pair
            startIdx = maxMinPair[0][x];
            endIdx = maxMinPair[1][x];

            // find the first presented peaks as secondary peaks (need some double checks when I am free)
            for (y = startIdx; y < endIdx; y++) {
                // extract the value
                v = secLastDiffFilterVal[y];
                w = secLastDiffFilterVal[y + 1];

                // skip some head and tail to prevent possible bug
                if (y < startIdx + 30 || y > endIdx - 50) {
                    continue;
                }
                // in second differential domain
                // record the value which is trough zero point
                // the value is transform from "positive" to "negative"
                if (v >= 0 && w <= 0) {
                    diffPeaksValAll[z] = mem[y + DiffFilterN / 2 + secDiffFilterN / 2 + 2];
                    diffPeaksIdxAll[z] = y + DiffFilterN / 2 + secDiffFilterN / 2 + 2;
                    diffPeaksPpdAll[z] = y + DiffFilterN / 2 + secDiffFilterN / 2 + 2 - startIdx; // unit: number of samples
                    z++;
                    break;
                }
            }
        }

        //=================================================================
        // calculate the SBP, DBP by Peaks Detection
        //=================================================================
        // set Blood Pressure Alogithm variable
        float height = 180;
        float L = height / 3;
        float mean_PPD0 = 0;
        float mean_PPD = 0;
        float Kc = 0;
        float Ka = 0;
        float SBP0 = mCalibrationSBP;
        float DBP0 = mCalibrationDBP;
        float nowSBP = 0;
        float nowDBP = 0;
        float lastSBP = 0;
        float lastDBP = 0;
        float[] peakSBPAll = new float[REGISTER];
        float[] peakDBPAll = new float[REGISTER];
        startIdx = 3;
        endIdx = z;
        int n = 0;
        int p = 0;
        int PDCount = 0;

        for (n = startIdx; n < endIdx; n++) {
            // mean 1~2 PPD as mean_PPD0
            mean_PPD0 = (float) (secPeaksPpdAll[n - 2] + secPeaksPpdAll[n - 1]) / SAMPLE_FREQUENCY / 2;
            // mean 3 PPD as mean_PPD
            mean_PPD = (float) (secPeaksPpdAll[n]) / SAMPLE_FREQUENCY;
            // Blood Pressure Alogithm
            Kc = (SBP0 - DBP0) * mean_PPD0 * mean_PPD0;
            Ka = DBP0 - 2 / 0.031f * (float) Math.log(L / mean_PPD0) + Kc / 3.0f / mean_PPD0 / mean_PPD0;
            // Calculate DBP value
            nowDBP = Ka + (2 / 0.031f * (float) Math.log(L / mean_PPD)) - (1.0f / 3.0f * Kc / mean_PPD / mean_PPD);
            // calculate SBP value
            nowSBP = nowDBP + Kc / mean_PPD / mean_PPD;
            // compare original SBP and DBP value to avoid bug
            if (nowDBP < DBP0 * 1.2 && nowDBP > DBP0 * 0.8 && nowSBP < SBP0 * 1.2 && nowSBP > SBP0 * 0.8) {
                // save SBP and DBP value
                peakSBPAll[PDCount] = nowSBP;
                peakDBPAll[PDCount] = nowDBP;
                PDCount++;
            }

        }

        //=================================================================
        // calculate the SBP, DBP by Differential Detection
        //=================================================================
        // reset Blood Pressure Algorithm variable
        mean_PPD0 = 0;
        mean_PPD = 0;
        Kc = 0;
        Ka = 0;
        SBP0 = mCalibrationSBP;
        DBP0 = mCalibrationDBP;
        nowSBP = 0;
        nowDBP = 0;
        lastSBP = 0;
        lastDBP = 0;
        float[] diffSBPAll = new float[REGISTER];
        float[] diffDBPAll = new float[REGISTER];
        startIdx = 3;
        endIdx = z;   // length(secPeaksPpdAll);
        n = 0;
        p = 0;
        int DDCount = 0;
        for (n = startIdx; n < endIdx; n++) {
            // mean 1~2 PPD as mean_PPD0
            mean_PPD0 = (float) (diffPeaksPpdAll[n - 2] + diffPeaksPpdAll[n - 1]) / SAMPLE_FREQUENCY / 2;
            // mean 3 PPD as mean_PPD
            mean_PPD = (float) (diffPeaksPpdAll[n]) / SAMPLE_FREQUENCY;
            // Blood Pressure Algorithm
            Kc = (SBP0 - DBP0) * mean_PPD0 * mean_PPD0;
            Ka = DBP0 - 2 / 0.031f * (float) Math.log(L / mean_PPD0) + Kc / 3 / mean_PPD0 / mean_PPD0;
            // calculate DBP value
            nowDBP = Ka + (2 / 0.031f * (float) Math.log(L / mean_PPD)) - (1.0f / 3.0f * Kc / mean_PPD / mean_PPD);
            // calculate SBP value
            nowSBP = nowDBP + Kc / mean_PPD / mean_PPD;
            // compare original SBP and DBP value to avoid bug
            if (nowDBP < DBP0 * 1.2 && nowDBP > DBP0 * 0.8 && nowSBP < SBP0 * 1.2 && nowSBP > SBP0 * 0.8) {
                // save SBP and DBP value
                diffSBPAll[DDCount] = nowSBP;
                diffDBPAll[DDCount] = nowDBP;
                DDCount++;
            }
        }

        //=================================================================
        // calculate the last SBP, DBP
        //=================================================================
        i = 0;
        j = 0;
        float PD = 0;
        float DD = 0;
        int k = 0;
        float PDmeanSBP = 0;
        float PDmeanDBP = 0;
        float DDmeanSBP = 0;
        float DDmeanDBP = 0;
        float PDSBPsum = 0;
        float PDDBPsum = 0;
        float DDSBPsum = 0;
        float DDDBPsum = 0;
        int OutPutSwitch = 10;
        // sum the Peak Detection SBP and DBP
        for (j = 0; j <= PDCount; j++) {
            PDSBPsum = PDSBPsum + peakSBPAll[j];
            PDDBPsum = PDDBPsum + peakDBPAll[j];
        }
        // mean the Peak Detection SBP and DBP
        PDmeanSBP = PDSBPsum / ((float) PDCount);
        PDmeanDBP = PDDBPsum / ((float) PDCount);
        // sum the Differential Detection SBP and DBP
        for (k = 0; k < DDCount; k++) {
            DDSBPsum = DDSBPsum + diffSBPAll[k];
            DDDBPsum = DDDBPsum + diffDBPAll[k];
        }
        // mean the Peak Detection SBP and DBP
        DDmeanSBP = DDSBPsum / ((float) DDCount);
        DDmeanDBP = DDDBPsum / ((float) DDCount);
        // calculated the error between Peak Detection and Differential Detection
        PD = (SBP0 - DDmeanSBP) * (SBP0 - DDmeanSBP);
        DD = (SBP0 - DDmeanSBP) * (SBP0 - DDmeanSBP);
        // if Peak Detection error is bigger than Differential, it will switch
        if (PD > DD)
            OutPutSwitch = 1;

        //=================================================================
        // calculate the HR
        //=================================================================
        // set HR variable
        float nowHR = 0;
        float nextHR = 0;
        float[] nowHRAll = new float[150];
        float[] HRAll = new float[150];
        startIdx = 1; // scan start from 5th data to prevent bug
        endIdx = numberFound;
        z = 0;
        j = 0;
        int HRFreq = 1;
        int HRCount = 0;
        float HRsum = 0;
        float meanHR = 0;

        for (z = startIdx; z <= endIdx - 1; z++) {
            // extract now max peaks Index
            nowMaxIdx = maxPeaksIdxAll[z];
            // extract next max peaks Index
            nextMaxIdx = maxPeaksIdxAll[z + 1];
            // use max peak index to calculate HR
            nowHR = 60 / (((float) nextMaxIdx - (float) nowMaxIdx) / (float) SAMPLE_FREQUENCY); // 60 = 1 minute
            // check the heart rate is between 48bmp and 120bmp
            // note: MIN_HR = 0.8; MAX_HR = 2;    /// unit: Hz
            if (48 <= nowHR && nowHR <= 120) {
                nowHRAll[HRCount] = nowHR;
                HRCount++;
            }
        }
        // Change output switch to nothing
        if (HRCount < 3) {
            OutPutSwitch = 0;
        }

        nowHR = 0;
        //=================================================================
        // Signal Quality Monitor
        //=================================================================
        for (HRCount = 2; HRCount < z; HRCount++) {
            nowHR = nowHRAll[HRCount - 1];
            nextHR = nowHRAll[HRCount];

            //check the heart rate is Periodic or not
            if (nowHR * 0.7 < nextHR && nextHR < nowHR * 1.3) {
                HRAll[HRFreq] = nowHR;
                HRFreq++;
            }
        }
        // Change output switch to nothing
        if (HRFreq == 1) {
            OutPutSwitch = 0;
        }

        // sum the HR value
        for (j = 1; j < HRFreq; j++) {
            HRsum = HRsum + HRAll[j];
        }

        // mean the HR value
        meanHR = HRsum / (HRFreq - 1);

        // the output value of HR
        mHR = (int) meanHR;

        // Change output switch to nothing
        if (mHR >= 120 || mHR <= 48) {
            OutPutSwitch = 0;
        }

        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        //                      Output and Environment AREA
        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        // if didn't find the possibility SBP,DBP
        // the output value will give the Calibration Value
        if (DDCount < 3 || OutPutSwitch == 0) {
            Log.d(TAG, "The signal is unstable! (" + OutPutSwitch + "," + DDCount + ")");
            mHR = -1;
            //mSBP = mCalibrationSBP;
            //mDBP = mCalibrationDBP;
        } else if (OutPutSwitch == 1 || PDCount < 3) {
            mSBP = (int) DDmeanSBP;
            mDBP = (int) DDmeanDBP;
        } else {
            mSBP = (int) PDmeanSBP;
            mDBP = (int) PDmeanDBP;
        }
    }

    public void setData(int data) {
        mData.add(data);
    }

    public void setCalibrationSBP(int sbp) {
        mCalibrationSBP = sbp;
    }

    public void setCalibrationDBP(int dbp) {
        mCalibrationDBP = dbp;
    }
}
