package com.sil.BFVSensing;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SimpleAdapter;
import android.widget.Toast;


import com.jjoe64.graphview.series.DataPoint;
import com.sil.BFVSensing.ChangeLanguage.Constant;
import com.sil.BFVSensing.ChangeLanguage.LangUtils;
import com.sil.BFVSensing.ChangeLanguage.SPUtils;
import com.sil.BFVSensing.FakeData.UploadFragment;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class Main extends AppCompatActivity {
    private static final String TAG = "SiL.Main";
    /* Icons for navigation */
    private int[] mIcons = {
            R.drawable.profile_icon_selector,
            R.drawable.measure_icon_selector,
            R.drawable.upload_icon_selector
    };
    //    private String[] tabicontext = {"資料","分析","上傳"};
    // Fragments
    private ProfileFragment mProfile;
    private MeasureFragment mMeasure;
    private CloudFragment mCloud;
    private UploadFragment mUpload;
    // Bluetooth check
    private BluetoothAdapter mBluetoothAdapter;
    private SimpleAdapter mLastAdapter;
    private ProgressDialog mProgressDialog;
    private static final String NAME = "name";
    private static final String ADDRESS = "number";
    // Bluetooth related variables
    private BluetoothDevice mPairedDevice;
    private BluetoothSocket mSocket;
    private OutputStream mOutputStream;
    private InputStream mInputStream;
    private Thread mMonitorThread;
    private volatile boolean mStopMonitor = false;
    private int mReadBufferPosition;
    private byte[] mReadBuffer;
    private Handler mEventHandler;
    private int tempData;
    private Dialog dialog;
    private Handler handler = new Handler();
    int xIndex;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
            } else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                mMeasure.mBTConnectionStatus.setText(getResources().getString(R.string.connect_with) + "\n" + getDeviceInfo(device));
                beginListenForData();
                mIsConnected = true;
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
            } else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                Log.d(TAG, "BT disconnect");
                mEventHandler.removeCallbacks(mPeriodicMeasuringRunnable);
                mMeasure.mBTConnectionStatus.setText(getResources().getString(R.string.disconnect_with) + "\n" + getDeviceInfo(device));
                if (mMeasure != null && mIsConnected && mStartMeasure) mMeasure.notifyDisconnect();
                mIsConnected = false;
            }
        }
    };

    // Create DataManager
    private DataManager mDataManager;
    private final Runnable mPeriodicMeasuringRunnable = new Runnable() {
        public void run() {
            mDataManager.calculateAverage();
            mEventHandler.postDelayed(mPeriodicMeasuringRunnable, 10000);
            mDataManager.clear();
        }
    };
    private boolean mStartMeasure = false;
    private MeasureFragment.MeasureCallback mCallback = new MeasureFragment.MeasureCallback() {
        @Override
        public void onStart() {
            synchronized (mCallback) {
                mStartMeasure = true;
                mCallback.notify();
                mMeasure.mMeasurementStatus.setText("");
            }
        }

        @Override
        public void onStop() {
            mStartMeasure = false;
            mIndex = 0;
        }

        @Override
        public void onCalculate() {
//            mDataManager.calculateAverage();
//            // if signal is unstable
//            if (mDataManager.getHR() == -1) {
//                mMeasure.mMeasurementStatus.setText("The signal is unstable!");
//            } else {
//                mMeasure.mMeasurementStatus.setText(" HR:  " + mDataManager.getHR() + "    SBP:  "
//                        + mDataManager.getSBP() + "    DBP:  " + mDataManager.getDBP());
//                mCloud.notifyFinishMeasure();
//            }
//            ColorDrawable[] color = {new ColorDrawable(0xFFFFFFFF), new ColorDrawable(0xFFF42D7A)};
//            TransitionDrawable trans = new TransitionDrawable(color);
//            mMeasure.mMeasurementStatus.setBackground(trans);
//            trans.startTransition(200);
//            trans.reverseTransition(200);
            mDataManager.clear();
        }

        @Override
        public void onForceStop() {
            mDataManager.clear();
            Toast.makeText(Main.this, getResources().getString(R.string.stop_measuring), Toast.LENGTH_LONG).show();
        }
    };

    private boolean mIsConnected = false;

    private int mIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //  Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        toolbar.inflateMenu(R.menu.menu_measuring);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                int id = item.getItemId();
                if (id == R.id.action_set_infinite) {
                    mMeasure.setMeasuringMode(!item.isChecked());
                    item.setChecked(!item.isChecked());
                    return true;
                } else if ((id == R.id.action_change_language)) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(Main.this);
                    alertDialog.setSingleChoiceItems(R.array.array_lang, SPUtils.getInstance(Constant.SP_NAME).getInt(Constant.SP_USER_LANG), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (SPUtils.getInstance(Constant.SP_NAME).getInt(Constant.SP_USER_LANG) == i) {
                                return;
                            }
                            dialog.dismiss();
                            changeLanguage(i);
                        }
                    })
                            .setNegativeButton(android.R.string.cancel, null);
                    dialog = alertDialog.create();
                    dialog.show();
                } else if (id == R.id.action_show_paired_devices) {
                    new AlertDialog.Builder(Main.this)
                            .setTitle("Paired device")
                            .setAdapter(getPairedDevices(), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    Log.d(TAG, "Connect to --> " +
                                            ((HashMap) mLastAdapter.getItem(which)).get(NAME).toString());
                                    mPairedDevice = mBluetoothAdapter.getRemoteDevice(
                                            ((HashMap) mLastAdapter.getItem(which)).get(ADDRESS).toString());
                                    mProgressDialog = new ProgressDialog(Main.this).show(
                                            Main.this,
                                            "Connecting...",
                                            "Connecting to " + mPairedDevice.getName() + ":" + mPairedDevice.getAddress(),
                                            true
                                    );
                                    mEventHandler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            mProgressDialog.dismiss();
                                        }
                                    }, 8000);
                                    openBT();
                                }
                            })
                            .show();
                } else if (id == R.id.action_show_data) {
                    item.setChecked(!item.isChecked());
                    mMeasure.showRealtimeData(item.isChecked());
                    return true;
                }
                return false;
            }
        });

//        /* Configure actionbar */
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setBackgroundDrawable(new ColorDrawable(0xFF2E8CF7));
//        actionBar.setTitle(Html.fromHtml("<font color='#ffffff'>BFV Sensing</font>"));

        /* Configure status bar */
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(0xFF2E8CF7);

        /* Initialize ViewPager and PageAdapter */
        TabsPagerAdapter adapter = new TabsPagerAdapter(getSupportFragmentManager(), setupContent());
        ViewPager pager = (ViewPager) findViewById(R.id.container);
        pager.setAdapter(adapter);
        pager.setCurrentItem(1);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(pager);
        setupNavigationIcons(tabLayout);
        setupNavigationIconsText(tabLayout);
        tabLayout.getTabAt(1).select();

        /* Initialize bluetooth adapter */
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        /* Receive bluetooth relevant broadcast */
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(mReceiver, filter);

        mDataManager = DataManager.getInstance();
        mEventHandler = new Handler();

        mMeasure.setMeasureCallback(mCallback);
    }

    private void setupNavigationIcons(TabLayout tabLayout) {
        for (int i = 0; i < mIcons.length; i++) {
            tabLayout.getTabAt(i).setIcon(mIcons[i]);
        }
    }

    private void setupNavigationIconsText(TabLayout tabLayout) {
        for (int i = 0; i < getResources().getStringArray(R.array.array_tabLayout_subtitle).length; i++) {
            tabLayout.getTabAt(i).setText(getResources().getStringArray(R.array.array_tabLayout_subtitle)[i]);
        }
    }

    private List<Fragment> setupContent() {
        List<Fragment> fragmentList = new ArrayList<Fragment>();
        mProfile = new ProfileFragment();
        mMeasure = new MeasureFragment();
//        mCloud = new CloudFragment();
        mUpload = new UploadFragment();
        fragmentList.add(mProfile);
        fragmentList.add(mMeasure);
//        fragmentList.add(mCloud);
        fragmentList.add(mUpload);
        return fragmentList;
    }


    private SimpleAdapter getPairedDevices() {
        if (mBluetoothAdapter != null) {
            // Get data
            List<Map<String, String>> devices = new ArrayList<>();
            for (BluetoothDevice device : mBluetoothAdapter.getBondedDevices()) {
                Map<String, String> map = new HashMap<>();
                map.put(NAME, device.getName());
                map.put(ADDRESS, device.getAddress());
                devices.add(map);
            }
            // Set adapter
            SimpleAdapter adapter = new SimpleAdapter(
                    Main.this,
                    devices,
                    android.R.layout.simple_list_item_2,
                    new String[]{NAME, ADDRESS},
                    new int[]{android.R.id.text1, android.R.id.text2});
            mLastAdapter = adapter;
            return adapter;
        }
        return null;
    }

    private void openBT() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    //Standard SerialPortService ID
                    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

                    mSocket = mPairedDevice.createRfcommSocketToServiceRecord(uuid);
                    mSocket.connect();
                    mOutputStream = mSocket.getOutputStream();
                    mInputStream = mSocket.getInputStream();
                } catch (IOException e) {
                    Log.e(TAG, e.toString());
                } finally {
                    // Dismiss running progress dialog
                    if (mProgressDialog != null) mProgressDialog.dismiss();
                }
            }
        }).start();
    }

    public void beginFakeData() {
        Thread mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                try {
                                    final int[] fakeDataArray = {};
                                    int[] arr = new int[1300];
                                    System.arraycopy(fakeDataArray, 0, arr, 0, 1300);
                                    mMeasure.notifyData(new DataPoint(mIndex, arr[xIndex]));
                                    if (xIndex > 1000) {
                                        xIndex = 0;
                                    } else {
                                        xIndex++;
                                    }
                                    mIndex++;
                                } catch (NumberFormatException e) {
                                    Log.e(TAG, e.toString());
                                }
                            }
                        });
                    }
                }, 0, 5);
            }
        });
        mThread.start();
    }


    private void beginListenForData() {
        //Looper.prepare();
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        mStopMonitor = false;
        mReadBufferPosition = 0;
        mReadBuffer = new byte[1024];
        mMonitorThread = new Thread(new Runnable() {
            public void run() {
                synchronized (mCallback) {
                    while (!Thread.currentThread().isInterrupted() && !mStopMonitor) {
                        try {
                            if (!mStartMeasure) {
                                Log.d(TAG, "Wait!");
                                mCallback.wait();
                            }

                            int bytesAvailable = mInputStream.available();
                            Log.d("bytesTesting", String.valueOf(bytesAvailable));

                            if (bytesAvailable > 0) {
                                byte[] packetBytes = new byte[bytesAvailable];
                                mInputStream.read(packetBytes);
                                for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[mReadBufferPosition];
                                        System.arraycopy(mReadBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        Log.d(TAG, "Testing: " + data);

                                        Thread a = new Thread(new Runnable() {
                                            public void run() {
                                                BufferedWriter bw;
                                                try {
                                                    // 取得網路輸出串流
                                                    bw = new BufferedWriter(new OutputStreamWriter(mProfile.clientSocket.getOutputStream()));
                                                    // 寫入訊息
                                                    String temp = "," + data;
                                                    bw.write(temp.trim());
                                                    // 立即發送
                                                    bw.flush();
                                                } catch (IOException e) {
                                                }
                                            }
                                        });
                                        a.start();

                                        mReadBufferPosition = 0;

                                        handler.post(new Runnable() {
                                            public void run() {
                                                try {
                                                    int dataForSeries = 0;
                                                    dataForSeries = Integer.parseInt(data.replaceAll("[^\\d]", ""));
                                                    mDataManager.setData(dataForSeries);
                                                    if (mStartMeasure && tempData % 6 == 0)
                                                        mMeasure.notifyData(new DataPoint(mIndex, dataForSeries));
                                                    mIndex++;
                                                    tempData++;
                                                } catch (NumberFormatException e) {
                                                    Log.e(TAG, e.toString());
                                                }
                                            }
                                        });
                                    } else {
                                        mReadBuffer[mReadBufferPosition++] = b;
                                    }
                                }
                            } else {
                                Log.d(TAG, "******等於0******");
                                mMonitorThread.sleep(200);
                            }
                        } catch (Exception ex) {
                            mStopMonitor = true;
                        }
                    }
                }
            }
        });
        mMonitorThread.start();
    }

    private String getDeviceInfo(BluetoothDevice device) {
        return device.getName() + ":" + device.getAddress();
    }

    public boolean isConnected() {
        return mIsConnected;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LangUtils.getAttachBaseContext(newBase, SPUtils.getInstance(Constant.SP_NAME).getInt(Constant.SP_USER_LANG)));
    }

    private void changeLanguage(int language) {
        //将选择的language保存到SP中
        SPUtils.getInstance(Constant.SP_NAME).put(Constant.SP_USER_LANG, language);
        if (mIsConnected) {
            try {
                mSocket.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            mMeasure.mBTConnectionStatus.setText(getResources().getString(R.string.connect_with) + "\n");
        }
        //重新启动Activity,并且要清空栈
        Intent intent = new Intent(this, Main.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
